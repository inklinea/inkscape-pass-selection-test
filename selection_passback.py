#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Inkscape Pass Selection Test
# A test project to show how a selection can be passed to a new instance of Inkscape
# An Inkscape 1.1+ extension
##############################################################################

import inkex
import tempfile, os, subprocess
from subprocess import Popen, PIPE, DEVNULL
from shutil import copy2
import random
import os
import shutil

DEVNULL = os.devnull

def build_id_list(self):
    rect_bool = self.options.select_rect_checkbox
    ellipse_bool = self.options.select_ellipse_checkbox
    path_bool = self.options.select_path_checkbox
    text_bool = self.options.select_text_checkbox

    my_id_list = []
    
    my_rect_ids = []
    if rect_bool == 'true':
        my_rectangles = self.svg.xpath('//svg:rect')
        for item in my_rectangles:
            my_rect_ids.append(item.attrib['id'])

    my_ellipse_ids = []
    if ellipse_bool == 'true':
        my_ellipses = self.svg.xpath('//svg:ellipse')
        for item in my_ellipses:
            my_ellipse_ids.append(item.attrib['id'])

    my_path_ids = []
    if path_bool == 'true':
        my_paths = self.svg.xpath('//svg:path')
        for item in my_paths:
            my_path_ids.append(item.attrib['id'])

    my_text_ids = []
    if text_bool == 'true':
        my_texts = self.svg.xpath('//svg:text')
        for item in my_texts:
            my_text_ids.append(item.attrib['id'])

    my_id_list += my_rect_ids + my_ellipse_ids + my_path_ids + my_text_ids
    my_id_list = ','.join(my_id_list)

    new_inkscape_with_selection(self, my_id_list)


def new_inkscape_with_selection(self, id_list):
    svg_filename = self.options.input_file
    temp_file_name = os.path.join(my_temp_folder, 'temp_selection_file_' + str(random.randrange(100000, 999999)) + '.svg')
    copy2(svg_filename, temp_file_name)
    Popen(['inkscape', '--with-gui', f'--actions=select-by-id:{id_list}', temp_file_name], stdout=PIPE, stderr=PIPE, encoding='utf8', errors='ignore')

class MyFirstExtension(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--select_rect_checkbox", type=str, dest="select_rect_checkbox")
        pars.add_argument("--select_ellipse_checkbox", type=str, dest="select_ellipse_checkbox")
        pars.add_argument("--select_path_checkbox", type=str, dest="select_path_checkbox")
        pars.add_argument("--select_text_checkbox", type=str, dest="select_text_checkbox")

    def effect(self):

        # Create a global temp folder
        global my_temp_folder
        my_temp_folder = tempfile.mkdtemp()

        #Send ID List
        build_id_list(self)

if __name__ == '__main__':
    MyFirstExtension().run()
